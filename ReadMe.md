# Advanced Radio Player

Radio Player with editable list of stations

## Screenshots

![Plasmoid full view](screenshots/preview.png)
![Plasmoid full view](screenshots/preview2.png)
![Plasmoid full view](screenshots/preview3.png)
![Plasmoid full view](screenshots/preview4.png)
![Plasmoid full view](screenshots/preview5.png)
![Plasmoid full view](screenshots/preview6.png)

## Features

* Add/delete/edit stations in the list
* Display album art (if possible) and more info for track
* Search for radio stations

## Requirements

* [Gstreamer and gstreamer-plugins](https://gstreamer.freedesktop.org/documentation/installing/on-linux.html?gi-language=c)
* Qt >= 5.12
* QtquickControls >= 2.12
* KDE Frameworks >= 5.89

## Installation

The preferred and easiest way to install is to use Plasma Discover or KDE Get New Stuff and search for *Advanced Radio Player*.

## Credits

### [Radio Browser](https://www.radio-browser.info) (API for search stations)

### [LastFM](https://www.last.fm) (Track recognition API)
