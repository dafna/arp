/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15

ListModel {
    id: stationsModel

    ListElement {
        name: "Example"
        hostname: "example.com"
        active: false
    }
}
