/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.20 as Kirigami
import Qt.labs.platform 1.1 as Labs
import org.kde.plasma.core 2.1 as PlasmaCore
import ".." as ARP

ColumnLayout {
    property string cfg_servers: plasmoid.configuration.servers
    property int dialogMode: -1

    Layout.fillWidth: true
    Layout.fillHeight: true

    Component.onCompleted: {
        stationsModel.clear()
        var servers = JSON.parse(cfg_servers)
        for (const server of servers) {
            stationsModel.append(server)
        }
    }

    Component {
        id: delegateComponent

        Kirigami.SwipeListItem {
            id: listItem

            width: scrollBar.visible ? mainList.width - scrollBar.width : mainList.width

            background: Rectangle {
                Kirigami.Theme.inherit: false
                Kirigami.Theme.colorSet: Kirigami.Theme.View
                color: listItem.hovered ? Kirigami.Theme.hoverColor : model
                                          && model.index % 2 ? Kirigami.Theme.alternateBackgroundColor : Kirigami.Theme.backgroundColor
                opacity: listItem.hovered ? 0.5 : 1.0
            }

            contentItem: RowLayout {
                clip: true

                QQC2.Label {
                    height: Math.max(implicitHeight, Kirigami.Units.iconSizes.smallMedium)
                    text: model ? model.index + 1 : ""
                    color: listItem.textColor
                }

                Kirigami.ListItemDragHandle {
                    listItem: listItem
                    listView: mainList
                    onMoveRequested: {
                        stationsModel.move(oldIndex, newIndex, 1)
                        cfg_servers = JSON.stringify(getServersArray())
                    }
                }

                Rectangle {
                    id: trackRect

                    clip: true
                    color: "transparent"

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.rightMargin: Kirigami.Units.smallSpacing
                    height: parent.height

                    QQC2.Label {
                        id: trackName

                        Layout.fillWidth: true

                        anchors.verticalCenter: parent.verticalCenter
                        height: Math.max(implicitHeight,
                                         Kirigami.Units.iconSizes.smallMedium)
                        text: model ? model.name : ""
                        color: listItem.textColor

                        XAnimator {
                            target: trackName
                            from: 0
                            to: -trackName.paintedWidth
                            duration: Math.round(
                                          Math.abs(to - from) / Kirigami.Units.gridUnit * 300
                                          * plasmoid.configuration.speedfactor)
                            running: listItem.containsMouse
                                     && trackName.width > trackRect.width
                            loops: 1

                            onFinished: {
                                from = trackRect.width
                                if (listItem.containsMouse) {
                                    start()
                                }
                            }
                            onStopped: {
                                from = 0
                                trackName.x = 0
                            }
                        }
                    }
                }
            }
            actions: [
                Kirigami.Action {
                    iconName: "edit-entry"
                    text: i18n("Edit")
                    onTriggered: {
                        mainList.currentIndex = model.index
                        editServer()
                    }
                },
                Kirigami.Action {
                    iconName: checked ? "password-show-on" : "password-show-off"
                    text: checked ? i18n("Hide") : i18n("Show")
                    checked: model ? model.active : false
                    checkable: true
                    onTriggered: {
                        model.active = checked
                        cfg_servers = JSON.stringify(getServersArray())
                    }
                },
                Kirigami.Action {
                    iconName: "delete"
                    text: i18n("Remove")
                    onTriggered: {
                        stationsModel.remove(model.index)
                        cfg_servers = JSON.stringify(getServersArray())
                    }
                }
            ]
        }
    }

    ARP.StationsModel {
        id: stationsModel
    }

    ListView {
        id: mainList

        Layout.fillWidth: true
        Layout.fillHeight: true
        clip: true

        QQC2.ScrollBar.vertical: QQC2.ScrollBar {
            id: scrollBar
        }

        Rectangle {
            anchors.fill: parent
            z: -1
            Kirigami.Theme.colorSet: Kirigami.Theme.View
            Kirigami.Theme.inherit: false
            color: Kirigami.Theme.backgroundColor
        }

        model: stationsModel
        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
        delegate: Kirigami.DelegateRecycler {
            sourceComponent: delegateComponent
        }
    }

    Kirigami.InlineMessage {
        id: importexportmessage

        property bool positive

        Layout.fillWidth: true

        visible: false
        showCloseButton: true
        type: positive ? Kirigami.MessageType.Positive : Kirigami.MessageType.Error
    }

    Timer {
        id: closetimer

        running: false
        repeat: false
        interval: 10000
        onTriggered: {
            importexportmessage.visible = false
        }
    }

    RowLayout {
        QQC2.Button {
            text: i18n("Add...")
            icon.name: "list-add"
            onClicked: addServer()
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Button {
            text: i18n("Import...")
            icon.name: "document-import"
            onClicked: openFileDialog.open()
        }

        QQC2.Button {
            text: i18n("Export...")
            icon.name: "document-export"
            onClicked: saveFileDialog.open()
        }
    }

    Kirigami.Dialog {
        id: serverDialog

        title: dialogMode === -1 ? i18n("Add Station") : i18n("Edit station")
        padding: Kirigami.Units.largeSpacing
        standardButtons: QQC2.Dialog.Ok | QQC2.Dialog.Cancel

        ColumnLayout {
            Kirigami.FormLayout {
                QQC2.TextField {
                    id: serverName
                    Kirigami.FormData.label: i18n("Name:")
                }

                QQC2.TextField {
                    id: serverHostname
                    Kirigami.FormData.label: i18n("URL:")
                }

                QQC2.CheckBox {
                    id: serverActive
                    checked: true
                    text: i18n("Active")
                }
            }
        }

        onOpened: serverName.forceActiveFocus(Qt.MouseFocusReason)

        onAccepted: {
            const itemObject = {
                "name": serverName.text,
                "hostname": serverHostname.text,
                "active": serverActive.checked,
            }

            if (dialogMode === -1) {
                stationsModel.append(itemObject)
            } else {
                stationsModel.set(dialogMode, itemObject)
            }

            cfg_servers = JSON.stringify(getServersArray())
        }
    }

    Labs.FileDialog {
        id: openFileDialog

        nameFilters: ["ARP Stations Backup (*.arp)"]
        fileMode: Labs.FileDialog.OpenFile
        folder: Labs.StandardPaths.writableLocation(Labs.StandardPaths.HomeLocation)

        onAccepted: {
            openFile(currentFile)
        }
    }

    Labs.FileDialog {
        id: saveFileDialog

        nameFilters: ["ARP Stations Backup (*.arp)"]
        folder: Labs.StandardPaths.writableLocation(Labs.StandardPaths.HomeLocation)
        fileMode: Labs.FileDialog.SaveFile

        onVisibleChanged: {
            if (visible) {
                const home = Labs.StandardPaths.writableLocation(Labs.StandardPaths.HomeLocation)
                currentFile = "file: ///" + home + "/stations.arp"
            }
        }
        onAccepted: saveFile(currentFile, cfg_servers)
    }

    function openFile(fileUrl: url) {
        const request = new XMLHttpRequest()
        request.open("GET", fileUrl, false)
        request.send(null)
        try {
            const servers = JSON.parse(request.responseText)
            console.log(request.responseText)
            stationsModel.clear()
            for (const srv of servers) {
                stationsModel.append(srv)
            }
            cfg_servers = JSON.stringify(getServersArray())
            showMessage(true, i18n("Сonfiguration has been loaded. Click 'Apply' to save changes."))
        } catch (e) {
            showMessage(false, i18n("Error loading configuration. Try choosing a different file."))
        }
    }

    function saveFile(fileUrl: url, content: string) {
        var file = fileUrl.toString().replace("file:///", "/")
        // content = content.replace(/'/g, "\\'")
        var xhr = new XMLHttpRequest()
        xhr.open("PUT", file, true)
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8')
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                checkFile(fileUrl)
            } else {
                showMessage(false, i18n("Error, make sure the selected directory is writable!"))
            }
        }
        xhr.send(content)
    }

    function checkFile(fileUrl: url) {
        var request = new XMLHttpRequest()
        request.open("GET", fileUrl, true)
        request.onreadystatechange = () => {
            if (request.readyState === 4 && request.status === 200) {
                if (cfg_servers === request.responseText) {
                    showMessage(true, i18n("You сonfiguration saved successfully."))
                } else {
                    showMessage(false, i18n("Error, make sure the selected directory is writable!"))
                }
            }
        }
        request.send(null)
    }

    function showMessage(positive: bool, text: string) {
        importexportmessage.positive = positive
        importexportmessage.text = text
        importexportmessage.visible = true
        closetimer.restart()
    }

    function getServersArray() {
        var serversArray = []
        for (let i = 0; i < stationsModel.count; i++) {
            serversArray.push(stationsModel.get(i))
        }
        return serversArray
    }

    function addServer() {
        dialogMode = -1
        serverName.text = ""
        serverHostname.text = ""
        serverActive.checked = true
        serverDialog.visible = true
        serverName.focus = true
    }

    function editServer() {
        dialogMode = mainList.currentIndex
        serverName.text = stationsModel.get(dialogMode).name
        serverHostname.text = stationsModel.get(dialogMode).hostname
        serverActive.checked = stationsModel.get(dialogMode).active
        serverDialog.visible = true
        serverName.focus = true
    }
}
