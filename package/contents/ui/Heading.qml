/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0

RowLayout {
    spacing: 0

    Rectangle {
        Layout.topMargin: Plasmoid.configuration.panel ? 0 : PlasmaCore.Units.smallSpacing
        Layout.bottomMargin: Plasmoid.configuration.panel ? 0 : PlasmaCore.Units.smallSpacing
        Layout.preferredWidth: Plasmoid.configuration.panel && Plasmoid.formFactor !== PlasmaCore.Types.Planar
            ? textMetrics.boundingRect.width + PlasmaCore.Units.smallSpacing * 2
            : fullRepresentation.width

        implicitHeight: Plasmoid.configuration.panel && Plasmoid.formFactor !== PlasmaCore.Types.Planar
            ? parent.height : heading.height

        color: "transparent"
        border.color: Plasmoid.configuration.border
                      && Plasmoid.configuration.panel
                      && Plasmoid.formFactor !== PlasmaCore.Types.Planar
            ? PlasmaCore.ColorScope.textColor : "transparent"

        radius: PlasmaCore.Units.smallSpacing
        clip: true

        onWidthChanged: {
            if (isPlaying()) {
                calculateAnimation()
            } else {
                anim.stop()
                heading.x = -heading.width / 2 + width / 2
            }
        }

        onHeightChanged: {
            if (Plasmoid.configuration.panel && Plasmoid.formFactor !== PlasmaCore.Types.Planar) {
                if (textMetrics.boundingRect.height >= height - PlasmaCore.Units.smallSpacing * 2) {
                    if (heading.level < 5) {
                        heading.level += 1
                    }
                } else {
                    if (heading.level > 1) {
                        heading.level -= 1
                    }
                }
                heading.x = -heading.width / 2 + width / 2
            }
        }

        TextMetrics {
            id: textMetrics

            font.family: PlasmaCore.Theme.defaultFont.family
            font.pointSize: heading.font.pointSize
            text: Plasmoid.title
        }

        PlasmaExtras.Heading {
            id: heading

            color: Plasmoid.userBackgroundHints === PlasmaCore.Types.ShadowBackground
                ? PlasmaCore.ColorScope.highlightedTextColor
                : PlasmaCore.ColorScope.textColor

            anchors.verticalCenter: parent.verticalCenter
            text: root.title
            level: 1
            maximumLineCount: 1

            onTextChanged: {
                if (isPlaying()) {
                    calculateAnimation()
                } else {
                    anim.stop()
                    heading.x = -heading.width / 2 + parent.width / 2
                }
            }

            Component.onCompleted: {
                x = -heading.width / 2 + parent.width / 2
            }
        }
    }

    XAnimator {
        id: anim

        target: heading
        easing.type: Easing.Linear
        duration: Math.round(Math.abs(to - from) / PlasmaCore.Units.gridUnit
                             * 300 * Plasmoid.configuration.speedfactor)
    }

    function calculateAnimation() {
        anim.from = width + PlasmaCore.Units.gridUnit
        anim.to = -heading.width
        anim.loops = Animation.Infinite
        anim.restart()
    }

    Connections {
        target: Plasmoid.configuration

        function onSpeedfactorChanged() {
            if (isPlaying()) {
                calculateAnimation()
            }
        }
    }
}
