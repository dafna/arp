��    M      �  g   �      �     �     �     �     �     �  
   �     �     �     �  	   �     �          !     =     K     R     [     `     m  ;   s  4   �     �  	   �     �     �            #   "     F  	   K     U  	   Z     d     t     �  	   �     �     �  %   �     �     �     �     �  $   �     	     $	     +	  	   :	     D	     S	     b	     n	     s	     	  !   �	     �	     �	  0   �	     �	     
     
     
  
   
  0   #
     T
     Y
  .   k
     �
  &   �
     �
     �
     �
     �
     �
     �
  >   �
  6  6     m     |     �     �  "   �     �     �               +     <  &   V  8   }  #   �     �     �     �  )        2  d   ?  X   �     �               -  
   ;  1   F  W   x     �  	   �     �     �  !     -   &     T     i     r     �  S   �     �          "     B  F   O     �  
   �     �     �      �     �     	     !  #   2     V  H   m     �  &   �  c   �     T     c     x  	   �     �  [   �       &     ]   8     �  D   �     �     �  
     %     
   8  
   C  i   N     ;       ,   J              D   8                         6   %   L   >   *   
               2              -      =   $   5   )      (       B             +             "       A      :   C   3         9      <             /   H   0   7       &       ?   1                !          K   I      E           	   F           M         G      4      .             '   #          @    Active Add Station Add... Advanced Radio Player Animation speed: Appearance Audio Behavior Bitrate: Bitrate:  Buffering... Check internet connection... Choose station and enjoy... Clear Results Codec: Country: Edit Edit station Error Error loading configuration. Try choosing a different file. Error, make sure the selected directory is writable! Error:  Export... Faster Favicon: Genre:  Get additional info Get list of stations
Please wait... Hide Homepage: Icon Import... Indicator Icon: Indicator Position: Info Language: Muted Name: Nothing found
Try changing your query Panel View: Panel settings Plasmoid Icon: Play Radio with editable list of stations Remove Search Search Station Search by Select Icon... Server status: Set Default Show Show border Show notification Show notification on track change Slower Station Info Station is added. Click 'Apply' to save changes. Stations Stop Stream URL: Tags: Track name Try to get more info for track using Last-FM API URL: Use middle button Use middle click for play last playing station Volume You сonfiguration saved successfully. country kBit/s language last check:  name tags Сonfiguration has been loaded. Click 'Apply' to save changes. Project-Id-Version: advancedradio
Report-Msgid-Bugs-To: https://invent.kde.org/saurov/arp
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Включен Добавить станцию Добавить... Advanced Radio Player Скорость анимации: Внешний вид Звук Поведение Битрейт: Битрейт:  Буферизация... Проверьте интернет... Выберите станцию и радуйтесь... Сбросить результат Кодек: Страна: Изменить... Редактировать станцию Ошибка Ошибка при импорте конфигурации. Выберите другой файл. Ошибка, убедитесь что у вас есть права на запись! Ошибка:  Экспорт... Быстрее Иконка: Жанр:  Дополнительная информация Получаем список станций
Пожалуйста подождите... Скрыть Сайт: Значок Импорт... Значок индикатора Расположение индикатора Информация Язык Выключен Название: Ничего не найдено
Попробуйте изменить запрос Вид на панели Настройки панели Значок плазмоида Играть Радио с редактируемым списком станций Удалить Поиск Поиск станций Поиск по Выберите значок... Статус: По-умолчанию Показать Показывать границу Уведомление Показывать уведомление при смене трека Медленнее Информация о станции Станция добавлена. Нажмите 'Применить' для сохранения. Станции Остановить URL потока: Тэги: Название трека Пытаться получить информацию о треке через Last-FM API URL: Средняя клавиша мыши Клик средней клавишей запускает последнюю станцию Громкость Ваша конфигурация успешно сохранена. стране кБит/с языку последняя проверка:  имени тэгам Конфигурация загружена. Нажмите 'Принять' для сохранения. 