��    8      �  O   �      �     �     �     �     �     	       	        "     /     L     h     v     }     �     �     �  ;   �  4   �       	        !     *  #   2     V  	   [  	   e     o  	   t     ~     �  %   �     �     �     �     �     �  	   �     �     �     �  0         1     :     ?     K     Q     V  &   ]     �     �     �     �     �     �  >   �  �  �     �	     �	     �	     �	     �	     �	     �	     
  $   
  "   :
     ]
     o
     v
     }
     �
     �
  G   �
  :   �
          $     0  	   8  &   B     i     q     w     �     �     �     �  *   �     �  
   �     �  	   �     �               %     ,  F   F  
   �     �     �  
   �     �     �  )   �     �     �                 	   $  P   .         7   	              )      0   1              
                                    3      !       '      (       *      4      &           .          ,             -             8   +                  #          "       2          /   %             $                    6      5    Active Add Station Add... Advanced Radio Player Audio Bitrate: Bitrate:  Buffering... Check internet connection... Choose station and enjoy... Clear Results Codec: Country: Edit Edit station Error Error loading configuration. Try choosing a different file. Error, make sure the selected directory is writable! Error:  Export... Favicon: Genre:  Get list of stations
Please wait... Hide Homepage: Import... Info Language: Muted Name: Nothing found
Try changing your query OK Play Remove Search Search Station Search by Server status: Show Station Info Station is added. Click 'Apply' to save changes. Stations Stop Stream URL: Tags: URL: Volume You сonfiguration saved successfully. country kBit/s language last check:  name tags Сonfiguration has been loaded. Click 'Apply' to save changes. Project-Id-Version: advancedradio
Report-Msgid-Bugs-To: https://invent.kde.org/saurov/arp
PO-Revision-Date: 2022-12-16 09:11-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 21.08.0
Plural-Forms: nplurals=2; plural=(n > 1);
 Ativa Adicionar estação Adicionar... Advanced Radio Player Áudio Taxa de bits: Taxa de bits:  Carregando... Verificar conexão com a Internet... Escolha a estação e aproveite... Limpar resultados Codec: País: Editar Editar estação Erro Erro ao carregar a configuração. Tente escolher um arquivo diferente. Erro, certifique-se de que a pasta seleciona é gravável! Erro:  Exportar... Ícone: Gênero:  Obtendo lista de estações
Aguarde... Ocultar Site: Importar... Informações Idioma: Mudo Nome: Nada encontrado
Tente alterar sua consulta OK Reproduzir Excluir Pesquisar Pesquisar estação Pesquisar por Status do servidor: Exibir Informação da estação Estação adicionada. Clique em 'Aplicar' para salvar as alterações. Estações Parar URL da transmissão: Etiquetas: URL: Volume Sua configuração foi salva com sucesso. país kBit/s idioma última verificação: nome etiquetas A configuração foi carregada. Clique em 'Aplicar' para salvar as alterações. 