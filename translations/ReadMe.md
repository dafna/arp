|  Locale  |  Lines  | % Done|
|----------|---------|-------|
| Template |      77 |       |
| pt_BR    |   55/77 |   71% |
| ru_RU    |   73/77 |   94% |
| uk_UA    |   56/77 |   72% |
